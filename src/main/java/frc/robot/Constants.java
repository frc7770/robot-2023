package frc.robot;

import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.math.trajectory.TrapezoidProfile;
import edu.wpi.first.wpilibj.util.Color;

public class Constants {

  // Colors
  public static Color COLOR_INFINITE_VIOLET = new Color(15, 0, 28);
    
  // Spark Max Motors
  public static final int SPARK_MAX_MOTOR = 99;
  public static final double SPARK_MAX_SPEED = .9;
  public static final int COPILOT_DRIVE_CONTROLLER = 1;
  public static final int DRIVE_CONTROLLER = 0;

  // Arm 
  public static final int SHOULDER_MOTOR = 19; //17
  public static final int ELBOW_MOTOR = 62; //21
  public static final double ARM_MAX_X_DISTANCE = 4;
  public static final double ARM_MAX_Y_DISTANCE = 4.5;
  public static final double ARM_SPEED = .6;
  public static final double SHOULDER_ENCODER_RATIO = .00185; // IMPORTANT Find this value
  public static final double ELBOW_ENCODER_RATIO = 2.193;//1.376; // IMPORTANT Find this value
  public static final double SHOULDER_ARM_LENGTH = 2.75; // Both in feet
  public static final double ELBOW_ARM_LENGTH = 3;
  public static final double GRIPPER_LENGTH = .792;
  public static final double ARM_TO_EDGE_LENGTH = 1.667;
  public static final double ROTATED_ARM_TO_EDHE_LENGHT = 0;
  public static final double ARM_TO_FLOOR_LENGTH = 2;


  // Intake Claw
  public static final int RIGHT_INTAKE_MOTOR = 30;
  public static final int LEFT_INTAKE_MOTOR = 11;
  public static final double GRIPPER_SPEED = 1;

  // Turret
  public static final int TURRET_MOTOR = 10;
  public static final double TURRET_SPEED = .3;
  
  // Mecanum Drive
  public static final int DRIVE_LEFT_FRONT = 2;
  public static final int DRIVE_LEFT_BACK = 6;
  public static final int DRIVE_RIGHT_FRONT = 3;
  public static final int DRIVE_RIGHT_BACK = 5; 

  public static final double MEC_SPEED = 1.5;
  public static final double ROTATE_SPEED = .4;

  public static final double TURRET_ENCODER_RATIO = 2.25; //.75

  public static final double TANK_DRIVE_ENCODER_RATIO = .065586;
  public static final double TANK_DRIVE_ENCODER_ROTATION_RATIO = 0.074; // guess

  // Frogger climber
  public static final double CLIMBER_SPEED = 1;
  public static final int PNEUMATIC_HUB_ID = 2;
  //ClimberMotors
  public static final int CLIMBER_RIGHT = 4; //??????????????????????
  public static final int CLIMBER_LEFT = 13; //??????????????????????

  // From SysId tool
  // concrete garage at 1101
  public static final double ksVolts = 0.16903; 
  public static final double kvVoltSecondsPerMeter = 1.4425; 
  public static final double kaVoltSecondsSquaredPerMeter = 0.23582;    
  public static final SimpleMotorFeedforward kFeedforward =   new SimpleMotorFeedforward(ksVolts, kvVoltSecondsPerMeter, kaVoltSecondsSquaredPerMeter);
  public static final double kPDriveVel = 0.20673; 

  public static final double kTrackwidthMeters = .762; 
  //  public static final DifferentialDriveKinematics kDriveKinematics =
  //      new DifferentialDriveKinematics(kTrackwidthMeters);

  public static final double kMaxSpeedMetersPerSecond = 3;
  public static final double kMaxAccelerationMetersPerSecondSquared = 1;

  public static final double kRamseteB = 2.0;
  public static final double kRamseteZeta = 0.7;

  public static final double wheelRadius = .47878; //.0762

  // Example value only - as above, this must be tuned for your drive!
  public static final double kPFrontLeftVel = 0.5;
  public static final double kPRearLeftVel = 0.5;
  public static final double kPFrontRightVel = 0.5;
  public static final double kPRearRightVel = 0.5;
  
  public static final class AutoConstants {
    public static final double kMaxSpeedMetersPerSecond = 3;
    public static final double kMaxAccelerationMetersPerSecondSquared = 3;
    public static final double kMaxAngularSpeedRadiansPerSecond = Math.PI;
    public static final double kMaxAngularSpeedRadiansPerSecondSquared = Math.PI;

    public static final double kPXController = 0.5;
    public static final double kPYController = 0.5;
    public static final double kPThetaController = 0.5;

    // Constraint for the motion profilied robot angle controller
    public static final TrapezoidProfile.Constraints kThetaControllerConstraints =
        new TrapezoidProfile.Constraints(
            kMaxAngularSpeedRadiansPerSecond, kMaxAngularSpeedRadiansPerSecondSquared);
  }
}
