// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.CvSink;
import edu.wpi.first.cscore.CvSource;
import edu.wpi.first.cscore.UsbCamera;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.ProfiledPIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.util.Units;
import edu.wpi.first.wpilibj.BuiltInAccelerometer;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj.interfaces.Accelerometer;
import edu.wpi.first.wpilibj.smartdashboard.Field2d;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.MecanumControllerCommand;
import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import frc.robot.arm.ArmSubsystem;
import frc.robot.arm.HighHeightPlaceCommand;
import frc.robot.arm.MidHeightPlaceCommand;
import frc.robot.arm.PickUpHeightCommand;
import frc.robot.arm.StowArmCommand;
import frc.robot.arm.XboxArmCommand;
import frc.robot.arm.ZeroArmCommand;
import frc.robot.drive.MecanumSubsystem;
import frc.robot.drive.TankDrive;
import frc.robot.drive.TankDriveCommand;
import frc.robot.drive.AutonomousBalancing;
import frc.robot.drive.AutonomousTankDriveCommand;
import frc.robot.drive.AutonomousTankRotateCommand;
import frc.robot.drive.DriveBackwardCommand;
import frc.robot.drive.DriveXYZCommand;
import frc.robot.drive.MecanumFieldCentricCommand;
import frc.robot.drive.MecanumResetAngleCommand;
import frc.robot.drive.ToggleBrakeCommand;
import frc.robot.drive.ZeroEncoderCommand;
import frc.robot.gripper.AutoGripperReverse;
import frc.robot.gripper.AutonomousGripperCommand;
import frc.robot.gripper.GripperIntake;
import frc.robot.gripper.GripperReverse;
import frc.robot.gripper.GripperSubsystem;
import frc.robot.gripper.TestGripperCommand;
import frc.robot.led.LEDController;
import frc.robot.photonvision.PhotonVision;
import frc.robot.sensors.ColorSensor;
import frc.robot.sensors.ConeDetection;
import frc.robot.sensors.CubeDetection;
import frc.robot.sensors.Gyroscope;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import edu.wpi.first.apriltag.AprilTagDetection;
import edu.wpi.first.apriltag.AprilTagDetector;
import java.util.ArrayList;
import java.util.List;

import frc.robot.sensors.LimeLight;
import frc.robot.sensors.Potentiometer;
import frc.robot.sensors.UltraSensor;
import frc.robot.sensors.VisionThreadSensor;
import frc.robot.sensors.VisionThreadSensor2;
import frc.robot.turret.AutoRotateTurretCommand;
import frc.robot.turret.TurretSubsystem;
import frc.robot.turret.XboxTurretCommand;
import frc.robot.turret.ZeroTurretAngle;

/**
 * The VM is configured to automatically run this class, and to call the functions corresponding to
 * each mode, as described in the TimedRobot documentation. If you change the name of this class or
 * the package after creating this project, you must also update the manifest file in the resource
 * directory.
 */
public class Robot extends TimedRobot {

  // Subsystems
  public static MecanumSubsystem mecanumSubsystem;
  //public static TankDrive tankDrive;
  public static ArmSubsystem armSubsystem;
  public static TurretSubsystem turretSubsystem;
  public static GripperSubsystem gripperSubsystem;
  //public static ClimberSubsystem climberSubsystem;
  //maticsSubsystem pneumatics;

  // Sensors
  public static PhotonVision photonVision;
  public static ColorSensor colorSensor0 = new ColorSensor();
  public static VisionThreadSensor vision;
  public static VisionThreadSensor2 vision2;
  public static Accelerometer accelerometer = new BuiltInAccelerometer();  
  //public static UltraSensor ultrasonic = new UltraSensor();
  public static UsbCamera camera0;
  //public static UsbCamera camera1;

  public static LimeLight limeLight = new LimeLight();
  public static Gyroscope gyro;
  public static Potentiometer potentiometer = new Potentiometer();
  //public static PhotonVision photonVision;

  // Commands
  private Command m_autonomousCommand;

  // Controls
  public static XboxController xboxController1 = new XboxController(Constants.COPILOT_DRIVE_CONTROLLER);
  public static Joystick joystick = new Joystick(Constants.DRIVE_CONTROLLER);

  //Xbox Controller Buttons
  public static JoystickButton aButton = new JoystickButton(xboxController1, 1);
  public static JoystickButton bButton = new JoystickButton(xboxController1, 2);
  public static JoystickButton xButton = new JoystickButton(xboxController1, 3);
  public static JoystickButton yButton = new JoystickButton(xboxController1, 4);
  public static JoystickButton lbButton = new JoystickButton(xboxController1, 5);
  public static JoystickButton rbButton = new JoystickButton(xboxController1, 6);
  public static JoystickButton backButton = new JoystickButton(xboxController1, 7);
  public static JoystickButton startButton = new JoystickButton(xboxController1, 8);
  public static JoystickButton leftJoystickPress = new JoystickButton(xboxController1, 9);
  public static JoystickButton rightJoystickPress = new JoystickButton(xboxController1, 10);

  // Joystick Controller Buttons
  public static JoystickButton triggerButton = new JoystickButton(joystick, 1);
  public static JoystickButton downButton = new JoystickButton(joystick, 2);
  public static JoystickButton leftButton = new JoystickButton(joystick, 3);
  public static JoystickButton rightButton = new JoystickButton(joystick, 4);
  public static JoystickButton leftTopLeft = new JoystickButton(joystick, 5);
  public static JoystickButton leftTopMiddle  = new JoystickButton(joystick, 6);
  public static JoystickButton leftTopRight = new JoystickButton(joystick, 7);
  public static JoystickButton leftBottomRight = new JoystickButton(joystick, 8);
  public static JoystickButton leftBottomMiddle = new JoystickButton(joystick, 9);
  public static JoystickButton leftBottomLeft = new JoystickButton(joystick, 10);
  public static JoystickButton rightTopRight = new JoystickButton(joystick, 11);
  public static JoystickButton rightTopMiddle  = new JoystickButton(joystick, 12);
  public static JoystickButton rightTopLeft = new JoystickButton(joystick, 13);
  public static JoystickButton rightBottomLeft = new JoystickButton(joystick, 14);
  public static JoystickButton rightBottomMiddle = new JoystickButton(joystick, 15);
  public static JoystickButton rightBottomRight = new JoystickButton(joystick, 16);


  // Logging
  public static final Field2d field = new Field2d();
  public static final SendableChooser<Command> m_chooser = new SendableChooser<>();
  public static final SendableChooser<Double> gripperSpeed = new SendableChooser<>();
  public static final SendableChooser<Double> balanceSpeed = new SendableChooser<>();

  /**
   * Method that runs once when the robot is power on.
   */
  @Override
  public void robotInit() {
    System.out.println("RobotInit");

    // ---------------------
    // Initialize subsystems
    // ---------------------
    mecanumSubsystem = new MecanumSubsystem();
    mecanumSubsystem.setDefaultCommand(new MecanumFieldCentricCommand());//new DriveXYZCommand(kDefaultPeriod, kDefaultPeriod, kDefaultPeriod));
    //climberSubsystem = new ClimberSubsystem();
    //climberSubsystfem.setDefaultCommand(new XboxClimberCallobrateCommand());
    //tankDrive = new TankDrive();
    //tankDrive.setDefaultCommand(new TankDriveCommand());
    armSubsystem = new ArmSubsystem();
    armSubsystem.setDefaultCommand(new XboxArmCommand());
    turretSubsystem = new TurretSubsystem();
    turretSubsystem.setDefaultCommand(new XboxTurretCommand());
    gripperSubsystem = new GripperSubsystem();
    gripperSubsystem.setDefaultCommand(new TestGripperCommand());


    // ----------
    // Decoration
    // ----------
    //setTestLEDS(Constants.COLOR_INFINITE_VIOLET);
    //setTestLEDS2(Color.kDarkRed);

    // -------
    // Sensors 
    // -------
    /*
    try {
      Robot.colorSensor0 = 
    } catch (Exception e) {
      // color sensor not found
    } */
    camera0 = CameraServer.startAutomaticCapture(0);
    camera0.setResolution(640, 480);
    camera0.setFPS(30);
    //camera1 = CameraServer.startAutomaticCapture(1);
    //photonVision = new PhotonVision();
    gyro = new Gyroscope();
    vision = new VisionThreadSensor(camera0, new CubeDetection());
    //vision2 = new VisionThreadSensor2(camera0, new ConeDetection());
    //Thread visionThread = new Thread(() -> apriltagVisionThreadProc());
    //visionThread.setDaemon(true);
    //visionThread.start();

    // -------
    // Logging
    // -------
    //SmartDashboard.putData("Field", field);

    // ----------------
    // Strategy Chooser
    // ----------------
    m_chooser.setDefaultOption("Path 1", path1());
    m_chooser.addOption("Path 2", path2());
    m_chooser.addOption("Path 3", path3());
    m_chooser.addOption("Path 4", path4());

    gripperSpeed.addOption("Speed1", .25);
    gripperSpeed.addOption("Speed2", .5);
    gripperSpeed.addOption("Speed3", .75);
    gripperSpeed.addOption("Speed4", 1.0);

    balanceSpeed.addOption("Speed1", .3);
    balanceSpeed.addOption("Speed2", .35);
    balanceSpeed.addOption("Speed3", .4);
    balanceSpeed.addOption("Speed4", .45);
    balanceSpeed.addOption("Speed5", .5);
    balanceSpeed.addOption("Speed6", .55);
    balanceSpeed.addOption("Speed7", .6);



    SmartDashboard.putData(m_chooser);
    SmartDashboard.putData(gripperSpeed);
    SmartDashboard.putData(balanceSpeed);
  }

  /**
   * Runs during each cycle
   */
  @Override
  public void robotPeriodic() {
    CommandScheduler.getInstance().run();
    //setTestLED4();
    //setTestLED5();

    try {
      colorSensor0.getBall();
      //colorSensor0.logToDashboard();
    } catch (NullPointerException npe) {
      // no color sensor
    }


    //limeLight.logToDashboard();
    //gyro.logToDashboard();
    //mecanumSubsystem.logToDashboard();
    //tankDrive.logToDashboard();
    armSubsystem.logToDashboard();
    //gripperSubsystem.gripperLEDs();
    //ultrasonic.logToDashboard();
    turretSubsystem.logToDashboard();
    //potentiometer.logToDashboard();
    SmartDashboard.putBoolean("Has Target Cube Camera", vision.hasTarget());
    SmartDashboard.putNumber("Angle to Cube", vision.getAngle());
    //SmartDashboard.putBoolean("Has Target Cone Camera", vision2.hasTarget());
    //SmartDashboard.putNumber("Angle to Cone", vision2.getAngle());
  }

  /** 
   * This function is run once each time the robot enters autonomous mode. 
   */
  @Override
  public void autonomousInit() {
    m_autonomousCommand = m_chooser.getSelected();

    CommandScheduler.getInstance().schedule(
      m_autonomousCommand
    );
  }

  /** 
   * This function is called periodically during autonomous. 
   */
  @Override
  public void autonomousPeriodic() {

  }

  /**
   * Runs when teleop is disabled.
   */
  @Override
  public void autonomousExit() {
    //setTestLEDS(Constants.COLOR_INFINITE_VIOLET); 
  }

  /** 
   * This function is called once each time the robot enters teleoperated mode. 
   */
  @Override
  public void teleopInit() {
    configureButtonBindings();
    //setTestLEDS(Color.kRed);
    //setTestLEDS2(Color.kOrange);

  }

  /** 
   * This function is called periodically during teleoperated mode. 
   */
  @Override
  public void teleopPeriodic() {
    //setTestLEDS3();
    //Scheduler.getInstance().run();
  }

  /**
   * Runs when teleop is disabled.
   */
  @Override
  public void teleopExit() {
    //setTestLEDS(Constants.COLOR_INFINITE_VIOLET); 
  }

  /** 
   * This function is called once each time the robot enters test mode.
   */
  @Override
  public void testInit() {
  }

  /** This function is called periodically during test mode. */
  @Override
  public void testPeriodic() {

  }

  private void configureButtonBindings() {
    downButton.onTrue(new ZeroArmCommand());
    rightButton.onTrue(new ZeroEncoderCommand());
    leftButton.onTrue(new ToggleBrakeCommand());

    leftBottomLeft.onTrue(new StowArmCommand().withTimeout(3));
    leftBottomMiddle.onTrue(new PickUpHeightCommand().withTimeout(3));

    rightTopRight.onTrue(new ZeroTurretAngle());
    //rightTopMiddle.onTrue(new AutonomousBalancing().withTimeout(10));

    //yButton.onTrue(new PneumaticsToggleClimberCommand());
    // Zero Climber Position
    //aButton.onTrue(new ZeroClimberPositionCommand());



    aButton.onTrue(new ParallelCommandGroup(
      new StowArmCommand().withTimeout(3),
      new SequentialCommandGroup(
        new WaitCommand(.75),
        new AutoRotateTurretCommand(0).withTimeout(6)
      )
    ));
    bButton.onTrue(new ParallelCommandGroup(
      new AutoRotateTurretCommand(180).withTimeout(6),
      new SequentialCommandGroup(
        new WaitCommand(.75),
        new PickUpHeightCommand().withTimeout(3)
      )
    ));
    xButton.onTrue(new MidHeightPlaceCommand().withTimeout(3));
    yButton.onTrue(new HighHeightPlaceCommand());
    rightJoystickPress.onTrue(new ZeroTurretAngle());
    rbButton.onTrue(new ZeroArmCommand());
    lbButton.onTrue(new AutoRotateTurretCommand(0));
    startButton.onTrue(new AutoRotateTurretCommand(180));
    backButton.onTrue(new AutoRotateTurretCommand(-180));
    leftJoystickPress.onTrue(new AutoRotateTurretCommand(0, true));


  }

  public void setTestLEDS(Color color) {
    System.out.println("Changing LED Colors");
    try {
      if (testLEDs == null) {
        testLEDs = new LEDController(0, 145); //two 31s one 27 and a 56 (57?)
      }
      testLEDs.solid(color);
    } catch (Exception e) {
      //TODO: handle exception
    }
  }
  public void setTestLEDS2(Color color) {
    System.out.println("Changing LED Colors");
    try {
      if (testLEDs2 == null) {
        testLEDs2 = new LEDController(2, 56); //114 total
      }
      testLEDs2.solid(color);
    } catch (Exception e) {
      //TODO: handle exception
    }
  }
  public void setTestLEDS3() {
    //System.out.println("Changing LED Colors");
    try {
      if (testLEDs == null) {
        testLEDs = new LEDController(0, 145); //two 31s one 27 and a 56 (57?)
      }
      testLEDs.oneOneFlash(Color.kRed, Color.kBlue, 31);
      //testLEDs.segments(Color.kDarkRed, 0, 31);
      //testLEDs.segments(Color.kGreen, 27, 56);
      //testLEDs.segments(Color.kRed, 83, 31);
      //testLEDs.segments(Color.kOrange, 114, 31);
    } catch (Exception e) {
      //TODO: handle exception
    }
  }

  public void setTestLED4() {
    try {
      if (testLEDs == null) {
        testLEDs = new LEDController(0, 145); //two 31s one 27 and a 56 (57?)
      }
      testLEDs.random(145);
      //testLEDs.oneOneFlash(Color.kRed, Color.kBlue, 31);
      //testLEDs.segments(Color.kDarkRed, 0, 31);
      //testLEDs.segments(Color.kGreen, 27, 56);
      //testLEDs.segments(Color.kRed, 83, 31);
      //testLEDs.segments(Color.kOrange, 114, 31);
    } catch (Exception e) {
      //TODO: handle exception
    }
  }

  public void setTestLED5() {
    try {
      if (testLEDs == null) {
        testLEDs = new LEDController(0, 145); //two 31s one 27 and a 56 (57?)
      }
      testLEDs.pulse(Color.kGreen, 0, 29); //Constants.COLOR_INFINITE_VIOLET
      testLEDs.pulseBackwards(Color.kGreen, 28, 29);
      testLEDs.pulse(Color.kGreen, 58, 28);
      testLEDs.pulseBackwards(Color.kGreen, 85, 30);
      
      //testLEDs.oneOneFlash(Color.kRed, Color.kBlue, 31);
      //testLEDs.segments(Color.kDarkRed, 0, 31);
      //testLEDs.segments(Color.kGreen, 27, 56);
      //testLEDs.segments(Color.kRed, 83, 31);
      //testLEDs.segments(Color.kOrange, 114, 31);
    } catch (Exception e) {
      //TODO: handle exception
    }
  }


  public static LEDController testLEDs;
  private static LEDController testLEDs2;

  /*/
  void apriltagVisionThreadProc() {
    AprilTagDetector detector = new AprilTagDetector();
    detector.addFamily("tag16h5", 0);
  
    // Set the resolution
    camera0.setResolution(640, 480);

    // Get a CvSink. This will capture Mats from the camera
    CvSink cvSink = CameraServer.getVideo();
    // Setup a CvSource. This will send images back to the Dashboard
    CvSource outputStream = CameraServer.putVideo("detect", 640, 480);

    // Mats are very memory expensive. Lets reuse this Mat.
    Mat mat = new Mat();
    Mat grayMat = new Mat();
    ArrayList<Integer> tags = new ArrayList<>();

    //
    Scalar outlineColor = new Scalar(0, 255, 0);
    Scalar xColor = new Scalar(0, 0, 255);

    // This cannot be 'true'. The program will never exit if it is. This
    // lets the robot stop this thread when restarting robot code or
    // deploying.
    while (!Thread.interrupted()) {
      // Tell the CvSink to grab a frame from the camera and put it
      // in the source mat.  If there is an error notify the output.
      if (cvSink.grabFrame(mat) == 0) {
        // Send the output the error.
        outputStream.notifyError(cvSink.getError());
        // skip the rest of the current iteration
        continue;
      }

      Imgproc.cvtColor(mat, grayMat, Imgproc.COLOR_RGB2GRAY);

      AprilTagDetection[] detections = detector.detect(grayMat);
      tags.clear();
      for (AprilTagDetection detection : detections) {

        tags.add(detection.getId());

        for (var i = 0; i <= 3; i++) {
          var j = (i + 1) % 4;
          var pt1 = new Point(detection.getCornerX(i), detection.getCornerY(i));
          var pt2 = new Point(detection.getCornerX(j), detection.getCornerY(j));
          SmartDashboard.putNumber("CenterY", detection.getCenterY());
          SmartDashboard.putNumber("CenterX", detection.getCenterX());
          SmartDashboard.putNumber("CornerY" + i, detection.getCornerY(i));
          SmartDashboard.putNumber("CornerX" + i, detection.getCornerX(i));
          Imgproc.line(mat, pt1, pt2, outlineColor, 2);
        }

        var cx = detection.getCenterX();
        var cy = detection.getCenterY();
        var ll = 10;
        Imgproc.line(mat, new Point(cx - ll, cy), new Point(cx + ll, cy), xColor, 2);
        Imgproc.line(mat, new Point(cx, cy - ll), new Point(cx, cy + ll), xColor, 2);
        Imgproc.putText(mat, Integer.toString(detection.getId()), new Point (cx + ll, cy), Imgproc.FONT_HERSHEY_SIMPLEX, 1, xColor, 3);
      }

      SmartDashboard.putString("tag", tags.toString());
      // Give the output stream a new image to display
      outputStream.putFrame(mat);
    }

    detector.close();
  }
  */


  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  /*
  public Command getAutonomousCommand(int pathNumber) {
    Trajectory exampleTrajectory = multiPaths(pathNumber);
    MecanumControllerCommand mecanumControllerCommand =
        new MecanumControllerCommand(
            exampleTrajectory,
            mecanumSubsystem::getPose,
            Constants.kFeedforward,
            mecanumSubsystem.m_kinematics,

            // Position contollers
            new PIDController(Constants.AutoConstants.kPXController, 0, 0),
            new PIDController(Constants.AutoConstants.kPYController, 0, 0),
            new ProfiledPIDController(
              Constants.AutoConstants.kPThetaController, 0, 0, Constants.AutoConstants.kThetaControllerConstraints),

            // Needed for normalizing wheel speeds
            Constants.AutoConstants.kMaxSpeedMetersPerSecond,

            // Velocity PID's
            new PIDController(Constants.kPFrontLeftVel, 0, 0),
            new PIDController(Constants.kPRearLeftVel, 0, 0),
            new PIDController(Constants.kPFrontRightVel, 0, 0),
            new PIDController(Constants.kPRearRightVel, 0, 0),
            mecanumSubsystem::getCurrentState,
            mecanumSubsystem::setDriveMotorControllersVolts, // Consumer for the output motor voltages
            mecanumSubsystem);

    // Reset odometry to the starting pose of the trajectory.
    mecanumSubsystem.resetOdometry(exampleTrajectory.getInitialPose());

    // Run path following command, then stop at the end.
    return mecanumControllerCommand.andThen(() -> mecanumSubsystem.drive(0, 0, 0, false)).andThen(new ToggleBrakeCommand());
  }

  public Trajectory multiPaths(int pathNumber) {
    // Create config for trajectory
    TrajectoryConfig config =
        new TrajectoryConfig(
                Constants.kMaxSpeedMetersPerSecond,
                Constants.kMaxAccelerationMetersPerSecondSquared)
            // Add kinematics to ensure max speed is actually obeyed
            .setKinematics(mecanumSubsystem.m_kinematics);

    // Go straight back
    if (pathNumber == 1) {
      return
        TrajectoryGenerator.generateTrajectory(
              // Start at the origin facing the +X direction
              new Pose2d(0, 0, new Rotation2d(Units.degreesToRadians(0))),
              // Pass through these two interior waypoints, making an 's' curve path
              List.of(), //new Translation2d(-3, 0), new Translation2d(-3, 1)),
              // End 3 meters straight ahead of where we started, facing forward
              new Pose2d(4.5, 4.5, new Rotation2d(Units.degreesToRadians(0))),
              config);
    }

    // J path onto ramp
    else if (pathNumber == 2) {
      return
        TrajectoryGenerator.generateTrajectory(
              // Start at the origin facing the +X direction
              new Pose2d(0, 0, new Rotation2d(180)),
              // Pass through these two interior waypoints, making an 's' curve path
              List.of(new Translation2d(-4.5, 0), new Translation2d(-4, -3)), //new Translation2d(-3, 0), new Translation2d(-3, 1)),
              // End 3 meters straight ahead of where we started, facing forward
              new Pose2d(0, -3, new Rotation2d(Units.degreesToRadians(180))),
              config);
    }

    // L Path onto ramp
    else if (pathNumber == 3) {
      return
        TrajectoryGenerator.generateTrajectory(
              // Start at the origin facing the +X direction
              new Pose2d(0, 0, new Rotation2d(180)),
              // Pass through these two interior waypoints, making an 's' curve path
              List.of(new Translation2d(-4.5, 0), new Translation2d(-4, 3)), //new Translation2d(-3, 0), new Translation2d(-3, 1)),
              // End 3 meters straight ahead of where we started, facing forward
              new Pose2d(0, 3, new Rotation2d(Units.degreesToRadians(180))),
              config);
    }

    // GO out and come back
    else if (pathNumber == 4) {
      return
        TrajectoryGenerator.generateTrajectory(
              // Start at the origin facing the +X direction
              new Pose2d(0, 0, new Rotation2d(180)),
              List.of(new Translation2d(-5.0, 0),  new Translation2d(-3, 0)),
              new Pose2d(-1.5, 0, new Rotation2d(Units.degreesToRadians(180))),
              config);
    }

    // NO MOVEMENT IF FALSE PATH ENTERED
    return TrajectoryGenerator.generateTrajectory(
      // Start at the origin facing the +X direction
      new Pose2d(0, 0, new Rotation2d(0)),
      // Pass through these two interior waypoints, making an 's' curve path
      List.of(new Translation2d(0, 0)), //new Translation2d(-3, 0), new Translation2d(-3, 1)),
      // End 3 meters straight ahead of where we started, facing forward
      new Pose2d(4.5, 0, new Rotation2d(Units.degreesToRadians(0))),
      config);
  } */


  public ParallelCommandGroup path2() {
    return new ParallelCommandGroup (
      new SequentialCommandGroup(
        new ToggleBrakeCommand(true),
        new ZeroEncoderCommand(),
        new AutonomousTankDriveCommand(-2).withTimeout(1.5),
        new MidHeightPlaceCommand().withTimeout(4),
        new MidHeightPlaceCommand(22.5, 5).withTimeout(2),
        new AutoGripperReverse().withTimeout(.5),
        new MidHeightPlaceCommand().withTimeout(1.5),
        new ZeroEncoderCommand(),
        new AutonomousTankDriveCommand(-1).withTimeout(.75),
        new StowArmCommand().withTimeout(2.5),
        new WaitCommand(1),
        new ZeroEncoderCommand(),
        new AutonomousTankDriveCommand(-5.0).withTimeout(4),
        new WaitCommand(1),
        new AutonomousTankRotateCommand(true).withTimeout(1),
        new ToggleBrakeCommand()
      ) 
    );
  } 

  public ParallelCommandGroup path1() {
    return new ParallelCommandGroup (
      new SequentialCommandGroup(
        new ZeroArmCommand(55),
        new HighHeightPlaceCommand().withTimeout(1.5),
        new AutoGripperReverse(true).withTimeout(.75),

        new ZeroEncoderCommand(),
        new ToggleBrakeCommand(true),
        
      new ParallelCommandGroup(
        new SequentialCommandGroup (
          new WaitCommand(2.25),
          new AutonomousTankDriveCommand(-4.6)
        ),
        new AutonomousGripperCommand(),
        new SequentialCommandGroup(
          new WaitCommand(0),
          new ParallelCommandGroup(
            new SequentialCommandGroup(
              new AutoRotateTurretCommand(180).withTimeout(2),
              new WaitCommand(0),
              new AutoRotateTurretCommand(0, true).withTimeout(2)
            ),
            new SequentialCommandGroup(
              new WaitCommand(0),
              new PickUpHeightCommand(-35).withTimeout(3)
            )
          )
        )
      ).withTimeout(5),

      new ParallelCommandGroup(
        //new StowArmCommand().withTimeout(3),
        new PickUpHeightCommand(25),
        new SequentialCommandGroup(
          new WaitCommand(1),
          new AutoRotateTurretCommand(0).withTimeout(3)
        ),
        new SequentialCommandGroup(
          new WaitCommand(1.5),
          new ZeroEncoderCommand(),
          new AutonomousTankDriveCommand(4.25).withTimeout(5)
        )
      ).withTimeout(4),     
      new ToggleBrakeCommand().withTimeout(.1),

      //new MidHeightPlaceCommand(),
      //new MidHeightPlaceCommand().withTimeout(1),
      new PickUpHeightCommand(-25).withTimeout(2),
      new AutoGripperReverse(false).withTimeout(.75)
      ) 
    );
  }

  public ParallelCommandGroup path3() {
    return new ParallelCommandGroup (
      new SequentialCommandGroup(
        new ZeroEncoderCommand(),
        new AutonomousTankDriveCommand(-2),
        new WaitCommand(.75),
        new ZeroEncoderCommand(),
        new AutonomousTankDriveCommand(-1.5),
        new AutonomousBalancing().withTimeout(10)
      )
    );
  }

  public ParallelCommandGroup path4() {
    return new ParallelCommandGroup (
      //new AutoRotateTurretCommand(vision.getAngle())
      new AutoRotateTurretCommand(0, true)
    );
  }

}



