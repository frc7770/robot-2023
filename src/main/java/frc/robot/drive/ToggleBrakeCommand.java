package frc.robot.drive;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class ToggleBrakeCommand extends CommandBase {
    boolean brake = false;
    public ToggleBrakeCommand() {
        //addRequirements(Robot.tankDrive);
    }

    public ToggleBrakeCommand(boolean tBrake) {
        //addRequirements(Robot.tankDrive);
        brake = tBrake;
    }

    @Override
    public void execute() {
        if (brake) {
            //Robot.tankDrive.setBrake();
        }
        else {
            //Robot.tankDrive.toggleBrake();
        }
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
