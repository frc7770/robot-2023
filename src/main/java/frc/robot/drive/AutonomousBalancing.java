package frc.robot.drive;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class AutonomousBalancing extends CommandBase {
    double currentPitch = 0;
    double previousPitch = 0;
    double changePitch = 0;
    
    public AutonomousBalancing() {
        //addRequirements(Robot.tankDrive);
        new ToggleBrakeCommand(true);
    }

    @Override
    public void execute() {
        currentPitch = Robot.gyro.change();
        changePitch = previousPitch - currentPitch;
        //Robot.tankDrive.drive((-(Robot.gyro.change()) * .02) * Robot.balanceSpeed.getSelected(), 0); 
        SmartDashboard.putNumber("Current Pitch", Robot.gyro.change()); 
        SmartDashboard.putNumber("Change in Pitch", changePitch);   
        previousPitch = currentPitch;   
        
    }

    @Override
    public boolean isFinished() {
        return Math.abs(Robot.gyro.change()) < 1.0; //&& Math.abs(changePitch) < .1;
    }

    @Override
    public void end(boolean interrupted) {
        //Robot.tankDrive.drive(0, 0);
        new ToggleBrakeCommand();
    }
}
