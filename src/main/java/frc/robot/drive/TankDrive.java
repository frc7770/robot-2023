package frc.robot.drive;

import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class TankDrive extends SubsystemBase  {
private final CANSparkMax leftFront = new CANSparkMax(Constants.DRIVE_LEFT_FRONT, MotorType.kBrushless);
  private final CANSparkMax rightFront = new CANSparkMax(Constants.DRIVE_RIGHT_FRONT, MotorType.kBrushless);
  private final CANSparkMax leftBack = new CANSparkMax(Constants.DRIVE_LEFT_BACK, MotorType.kBrushless);
  private final CANSparkMax rightBack = new CANSparkMax(Constants.DRIVE_RIGHT_BACK, MotorType.kBrushless);  

  private final RelativeEncoder m_frontLeftEncoder = leftFront.getEncoder();
  private final RelativeEncoder m_frontRightEncoder = rightFront.getEncoder();
  private final RelativeEncoder m_backLeftEncoder = leftBack.getEncoder();
  private final RelativeEncoder m_backRightEncoder = rightBack.getEncoder();

  public TankDrive() {
    //rightBack.setInverted(true);
    //rightFront.setInverted(true);

    m_frontLeftEncoder.setPosition(0);
    m_backLeftEncoder.setPosition(0);
    m_frontRightEncoder.setPosition(0);
    m_backRightEncoder.setPosition(0);
  }

  public void drive(double speed, double rotate) {
    if (Math.abs(speed) > Math.abs(rotate) && Math.abs(speed) > .1) {
        leftFront.set(speed * Constants.MEC_SPEED);
        leftBack.set(speed * Constants.MEC_SPEED);
        rightFront.set(-speed * Constants.MEC_SPEED);
        rightBack.set(-speed * Constants.MEC_SPEED);
    }
    else if (Math.abs(rotate) > Math.abs(speed) && rotate > .1) {
      leftFront.set(rotate * Constants.ROTATE_SPEED);
      leftBack.set(rotate * Constants.ROTATE_SPEED);
      rightFront.set(rotate * Constants.ROTATE_SPEED);
      rightBack.set(rotate * Constants.ROTATE_SPEED);
    }
    else if (Math.abs(rotate) > Math.abs(speed) && rotate < .1) {
        leftFront.set(rotate * Constants.ROTATE_SPEED);
        leftBack.set(rotate * Constants.ROTATE_SPEED);
        rightFront.set(rotate * Constants.ROTATE_SPEED);
        rightBack.set(rotate * Constants.ROTATE_SPEED);
    }
    else {
        leftFront.set(0);
        leftBack.set(0);
        rightFront.set(0);
        rightBack.set(0);
    }
  }

  public void autoDrive(double speed, double rotate) {
    leftFront.set(speed);// * Constants.MEC_SPEED);
    leftBack.set(speed);// * Constants.MEC_SPEED);
    rightFront.set(-speed);// * Constants.MEC_SPEED);
    rightBack.set(-speed);// * Constants.MEC_SPEED);
  }

  public void setEncodersZero() {
    m_backLeftEncoder.setPosition(0);
    m_backRightEncoder.setPosition(0);
    m_frontLeftEncoder.setPosition(0);
    m_frontRightEncoder.setPosition(0);
  }

  public double getLeftEncoder() {
    return m_backLeftEncoder.getPosition();
  }

  public void logToDashboard() {
    SmartDashboard.putNumber("Left F Encoder", m_frontLeftEncoder.getPosition());
    SmartDashboard.putNumber("Left B Encoder", m_backLeftEncoder.getPosition());
    SmartDashboard.putNumber("Right F Encoder", m_frontRightEncoder.getPosition());
    SmartDashboard.putNumber("Right B Encoder", m_backRightEncoder.getPosition());
  }

  public void toggleBrake() {
    if (leftFront.getIdleMode() == IdleMode.kBrake) {
      leftFront.setIdleMode(IdleMode.kCoast);
      leftBack.setIdleMode(IdleMode.kCoast);
      rightFront.setIdleMode(IdleMode.kCoast);
      rightBack.setIdleMode(IdleMode.kCoast);
    }
    else {
      leftFront.setIdleMode(IdleMode.kBrake);
      leftBack.setIdleMode(IdleMode.kBrake);
      rightFront.setIdleMode(IdleMode.kBrake);
      rightBack.setIdleMode(IdleMode.kBrake);
    }
  }

  public void setBrake() {
    leftFront.setIdleMode(IdleMode.kBrake);
    leftBack.setIdleMode(IdleMode.kBrake);
    rightFront.setIdleMode(IdleMode.kBrake);
    rightBack.setIdleMode(IdleMode.kBrake);
  }
}
