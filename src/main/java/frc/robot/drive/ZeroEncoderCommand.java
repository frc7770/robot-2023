package frc.robot.drive;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class ZeroEncoderCommand extends CommandBase {
    public ZeroEncoderCommand() {
        //addRequirements(Robot.tankDrive);
    }

    @Override
    public void execute() {
        //Robot.tankDrive.setEncodersZero();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
