package frc.robot.drive;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;
import frc.robot.sensors.Gyroscope;

public class MecanumResetAngleCommand extends CommandBase {
    public MecanumResetAngleCommand() {
        addRequirements(Robot.mecanumSubsystem);
    }
    
    public void execute() {
        Robot.mecanumSubsystem.reset();
    }

    @Override
    public boolean isFinished() {
        return true;
    }
}
