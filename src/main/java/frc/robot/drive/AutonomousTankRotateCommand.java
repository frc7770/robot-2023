package frc.robot.drive;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class AutonomousTankRotateCommand extends CommandBase {
    boolean right = false;

    public AutonomousTankRotateCommand(boolean goRight) {
        //addRequirements(Robot.tankDrive);
        right = goRight;
    }

    @Override
    public void execute() {
        if (right) {
            //Robot.tankDrive.drive(0, .5);
        }
        else {
            //Robot.tankDrive.drive(0, -.5);
        }
    }

    public double getCurrentDegrees() {
        return 0;//Robot.tankDrive.getLeftEncoder() * Constants.TANK_DRIVE_ENCODER_ROTATION_RATIO;
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        //Robot.tankDrive.drive(0, 0);
    }
}
