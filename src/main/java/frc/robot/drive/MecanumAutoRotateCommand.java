package frc.robot.drive;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class MecanumAutoRotateCommand extends CommandBase {
    public static double BottomLeftCornerY;
  public static double BottomRightCornerY;
  public static double TopLeftCornerY;
  public static double TopRightCornerY;

  public static double BottomLeftCornerX;
  public static double BottomRightCornerX;
  public static double TopLeftCornerX;
  public static double TopRightCornerX;

  public static double diffCornerY;
  public static double diffCornerX;

    public MecanumAutoRotateCommand() {
        addRequirements(Robot.mecanumSubsystem);
    }
    @Override
    public void execute() {
        BottomLeftCornerY = SmartDashboard.getNumber("CornerY0", -1);
        BottomRightCornerY = SmartDashboard.getNumber("CornerY1", -1);
        TopRightCornerY = SmartDashboard.getNumber("CornerY2", -1);
        TopLeftCornerY = SmartDashboard.getNumber("CornerY3", -1);

        BottomLeftCornerX = SmartDashboard.getNumber("CornerX0", -1);
        BottomRightCornerX = SmartDashboard.getNumber("CornerX1", -1);
        TopRightCornerX = SmartDashboard.getNumber("CornerX2", -1);
        TopLeftCornerX = SmartDashboard.getNumber("CornerX3", -1);


        diffCornerY = (BottomLeftCornerY - TopLeftCornerY) - (BottomRightCornerY - TopRightCornerY);
        diffCornerX = ((BottomRightCornerX - BottomLeftCornerX) + (TopRightCornerX - TopLeftCornerX))/2;

        SmartDashboard.putNumber("DiffcornerY", diffCornerY);
        SmartDashboard.putNumber("DiffCornerX", diffCornerX);

        if (!(SmartDashboard.getString("tag", "[]").equals("[]"))) { 
            if (diffCornerY > 5) {
                //Robot.mecanumDriveSubsystem.zLargest(.25 * (-diffCornerY)/(diffCornerX));
                SmartDashboard.putNumber("Rotate Speed Right", (-diffCornerY)/(diffCornerX));
            }
            else if (diffCornerY < -5) {
                //Robot.mecanumDriveSubsystem.zLargest(.25 * (-diffCornerY)/(diffCornerX));
                SmartDashboard.putNumber("Rotate Speed Left", (-diffCornerY)/(diffCornerX));
            }
        }
        else {
            //Robot.mecanumDriveSubsystem.allZero();
            SmartDashboard.putNumber("Rotate Speed Zero", 0);
        }
    }
    
    @Override
    public boolean isFinished() {
       return false;
    }
}




    



