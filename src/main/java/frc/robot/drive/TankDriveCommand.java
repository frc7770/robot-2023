package frc.robot.drive;

import frc.robot.Robot;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;


public class TankDriveCommand extends CommandBase {
    public TankDriveCommand() {
        //addRequirements(Robot.tankDrive);
    }

    @Override
    public void execute() {
        double y = Robot.xboxController1.getRightY();
        double z = Robot.xboxController1.getRightX();

        //Robot.tankDrive.drive(-y, z);
    }


}
