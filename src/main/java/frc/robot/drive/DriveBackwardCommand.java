package frc.robot.drive;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class DriveBackwardCommand extends CommandBase {
    public DriveBackwardCommand() {
        addRequirements(Robot.mecanumSubsystem);
    }

    @Override
    public void execute() {
        Robot.mecanumSubsystem.drive(-.5, 0, 0, false);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        Robot.mecanumSubsystem.drive(0, 0, 0, false);
    }
}
