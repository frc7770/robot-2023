package frc.robot.drive;

import com.kauailabs.navx.frc.AHRS;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.hal.simulation.RelayDataJNI;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.MecanumDriveKinematics;
import edu.wpi.first.math.kinematics.MecanumDriveMotorVoltages;
import edu.wpi.first.math.kinematics.MecanumDriveOdometry;
import edu.wpi.first.math.kinematics.MecanumDriveWheelPositions;
import edu.wpi.first.math.kinematics.MecanumDriveWheelSpeeds;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.motorcontrol.MotorController;
import edu.wpi.first.wpilibj.motorcontrol.PWMSparkMax;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Robot;
import frc.robot.sensors.Gyroscope;

public class MecanumSubsystem extends SubsystemBase {
  public Gyroscope gyro = new Gyroscope();

  public static final double kMaxSpeed = 6.0; // 3 meters per second
  public static final double kMaxAngularSpeed = 2 * Math.PI; // 1/2 rotation per second

  private final CANSparkMax leftFront = new CANSparkMax(Constants.DRIVE_LEFT_FRONT, MotorType.kBrushless);
  private final CANSparkMax rightFront = new CANSparkMax(Constants.DRIVE_RIGHT_FRONT, MotorType.kBrushless);
  private final CANSparkMax leftBack = new CANSparkMax(Constants.DRIVE_LEFT_BACK, MotorType.kBrushless);
  private final CANSparkMax rightBack = new CANSparkMax(Constants.DRIVE_RIGHT_BACK, MotorType.kBrushless);  

  private final RelativeEncoder m_frontLeftEncoder = leftFront.getEncoder();
  private final RelativeEncoder m_frontRightEncoder = rightFront.getEncoder();
  private final RelativeEncoder m_backLeftEncoder = leftBack.getEncoder();
  private final RelativeEncoder m_backRightEncoder = rightBack.getEncoder();

  private final Translation2d m_frontLeftLocation = new Translation2d(0.381, 0.381);
  private final Translation2d m_frontRightLocation = new Translation2d(0.381, -0.381);
  private final Translation2d m_backLeftLocation = new Translation2d(-0.381, 0.381);
  private final Translation2d m_backRightLocation = new Translation2d(-0.381, -0.381);

  private final PIDController m_frontLeftPIDController = new PIDController(1, 0, 0);
  private final PIDController m_frontRightPIDController = new PIDController(1, 0, 0);
  private final PIDController m_backLeftPIDController = new PIDController(1, 0, 0);
  private final PIDController m_backRightPIDController = new PIDController(1, 0, 0);

  public final MecanumDriveKinematics m_kinematics = new MecanumDriveKinematics(
      m_frontLeftLocation, m_frontRightLocation, m_backLeftLocation, m_backRightLocation);

  private final MecanumDriveOdometry m_odometry = new MecanumDriveOdometry(m_kinematics, gyro.getRotation(),
      getCurrentDistances());

  // Gains are for example purposes only - must be determined for your own robot!
  private final SimpleMotorFeedforward m_feedforward = new SimpleMotorFeedforward(Constants.ksVolts, Constants.kvVoltSecondsPerMeter);

  /** Constructs a MecanumDrive and resets the gyro. */
  public MecanumSubsystem() {
    gyro.reset();
    // We need to invert one side of the drivetrain so that positive voltages
    // result in both sides moving forward. Depending on how your robot's
    // gearbox is constructed, you might have to invert the left side instead.
    rightBack.setInverted(true);
    rightFront.setInverted(true);

    m_frontLeftEncoder.setVelocityConversionFactor((2 * Math.PI * Constants.wheelRadius * .0254) / 60);
    m_frontRightEncoder.setVelocityConversionFactor((2 * Math.PI * Constants.wheelRadius * .0254) / 60);
    m_backLeftEncoder.setVelocityConversionFactor((2 * Math.PI * Constants.wheelRadius * .0254) / 60);
    m_backRightEncoder.setVelocityConversionFactor((2 * Math.PI * Constants.wheelRadius * .0254) / 60);

    m_frontLeftEncoder.setPositionConversionFactor(2 * Math.PI * Constants.wheelRadius * .0254);
    m_frontRightEncoder.setPositionConversionFactor(2 * Math.PI * Constants.wheelRadius * .0254);
    m_backLeftEncoder.setPositionConversionFactor(2 * Math.PI * Constants.wheelRadius * .0254);
    m_backRightEncoder.setPositionConversionFactor(2 * Math.PI * Constants.wheelRadius * .0254);

    m_frontLeftEncoder.setPosition(0);
    m_backLeftEncoder.setPosition(0);
    m_frontRightEncoder.setPosition(0);
    m_backRightEncoder.setPosition(0);
  }

  /**
   * Returns the current state of the drivetrain.
   *
   * @return The current state of the drivetrain.
   */
  public MecanumDriveWheelSpeeds getCurrentState() {
    return new MecanumDriveWheelSpeeds(
        m_frontLeftEncoder.getVelocity(),
        m_frontRightEncoder.getVelocity(),
        m_backLeftEncoder.getVelocity(),
        m_backRightEncoder.getVelocity());
  }

  /**
   * Returns the current distances measured by the drivetrain.
   *
   * @return The current distances measured by the drivetrain.
   */
  public MecanumDriveWheelPositions getCurrentDistances() {
    return new MecanumDriveWheelPositions(
        m_frontLeftEncoder.getPosition(),
        m_frontRightEncoder.getPosition(),
        m_backLeftEncoder.getPosition(),
        m_backRightEncoder.getPosition());
  }

  public void setEncodersZero() {
    m_backLeftEncoder.setPosition(0);
    m_backRightEncoder.setPosition(0);
    m_frontLeftEncoder.setPosition(0);
    m_frontRightEncoder.setPosition(0);
  }

  /**
   * Set the desired speeds for each wheel.
   *
   * @param speeds The desired wheel speeds.
   */
  public void setSpeeds(MecanumDriveWheelSpeeds speeds) {
    final double frontLeftFeedforward = m_feedforward.calculate(speeds.frontLeftMetersPerSecond);
    final double frontRightFeedforward = m_feedforward.calculate(speeds.frontRightMetersPerSecond);
    final double backLeftFeedforward = m_feedforward.calculate(speeds.rearLeftMetersPerSecond);
    final double backRightFeedforward = m_feedforward.calculate(speeds.rearRightMetersPerSecond);

    final double frontLeftOutput = m_frontLeftPIDController.calculate(
        m_frontLeftEncoder.getVelocity(), speeds.frontLeftMetersPerSecond);
    final double frontRightOutput = m_frontRightPIDController.calculate(
        m_frontRightEncoder.getVelocity(), speeds.frontRightMetersPerSecond);
    final double backLeftOutput = m_backLeftPIDController.calculate(
        m_backLeftEncoder.getVelocity(), speeds.rearLeftMetersPerSecond);
    final double backRightOutput = m_backRightPIDController.calculate(
        m_backRightEncoder.getVelocity(), speeds.rearRightMetersPerSecond);

    leftFront.setVoltage(/* frontLeftOutput */ +frontLeftFeedforward * Constants.MEC_SPEED);
    rightFront.setVoltage(/* frontRightOutput */ +frontRightFeedforward * Constants.MEC_SPEED);
    leftBack.setVoltage(/* backLeftOutput */ +backLeftFeedforward * Constants.MEC_SPEED);
    rightBack.setVoltage(/* backRightOutput */ +backRightFeedforward * Constants.MEC_SPEED);
  }

  /** Sets the front left drive MotorController to a voltage. */
  public void setDriveMotorControllersVolts(MecanumDriveMotorVoltages volts) {
    leftFront.setVoltage(volts.frontLeftVoltage);
    leftBack.setVoltage(volts.rearLeftVoltage);
    rightFront.setVoltage(volts.frontRightVoltage);
    rightBack.setVoltage(volts.rearRightVoltage);
  }

  /**
   * Method to drive the robot using joystick info.
   *
   * @param xSpeed        Speed of the robot in the x direction (forward).
   * @param ySpeed        Speed of the robot in the y direction (sideways).
   * @param rot           Angular rate of the robot.
   * @param fieldRelative Whether the provided x and y speeds are relative to the
   *                      field.
   */
  public void drive(double xSpeed, double ySpeed, double rot, boolean fieldRelative) {
    var mecanumDriveWheelSpeeds = m_kinematics.toWheelSpeeds(
        fieldRelative
            ? ChassisSpeeds.fromFieldRelativeSpeeds(xSpeed, ySpeed, rot, gyro.getRotation())
            : new ChassisSpeeds(xSpeed, ySpeed, rot));
    mecanumDriveWheelSpeeds.desaturate(kMaxSpeed);
    setSpeeds(mecanumDriveWheelSpeeds);
  }

  /** Updates the field relative position of the robot. */
  public void updateOdometry() {
    m_odometry.update(gyro.getRotation(), getCurrentDistances());
  }

  public void reset() {
    gyro.reset();
    setEncodersZero();
    updateOdometry();
   }

  /**
   * Returns the currently-estimated pose of the robot.
   *
   * @return The pose.
   */
  public Pose2d getPose() {
    return m_odometry.getPoseMeters();
  }

  /**
   * Resets the odometry to the specified pose.
   *
   * @param pose The pose to which to set the odometry.
   */
  public void resetOdometry(Pose2d pose) {
    m_odometry.resetPosition(gyro.getRotation(), getCurrentDistances(), pose);
  }

  public void periodic() {
    updateOdometry();
    if(Robot.field!=null) {
      Robot.field.setRobotPose(m_odometry.getPoseMeters());
      //Robot.field.setRobotPose(m_odometry.getPoseMeters().getX(), m_odometry.getPoseMeters().getY(), Rotation2d.fromDegrees(22));
    }
    SmartDashboard.putData(Robot.field);
  }

  public void toggleBrake() {
    if (leftFront.getIdleMode() == IdleMode.kBrake) {
      leftFront.setIdleMode(IdleMode.kCoast);
      leftBack.setIdleMode(IdleMode.kCoast);
      rightFront.setIdleMode(IdleMode.kCoast);
      rightBack.setIdleMode(IdleMode.kCoast);
    }
    else {
      leftFront.setIdleMode(IdleMode.kBrake);
      leftBack.setIdleMode(IdleMode.kBrake);
      rightFront.setIdleMode(IdleMode.kBrake);
      rightBack.setIdleMode(IdleMode.kBrake);
    }
  }

  public void logToDashboard() {
    SmartDashboard.putNumber("Z Angle", gyro.getAngle());
    SmartDashboard.putString("Rotation", gyro.getRotation().toString());
    SmartDashboard.putString("Pose", getPose().toString());


    SmartDashboard.putNumber("Left F Encoder Rotations", m_frontLeftEncoder.getPosition()/(2 * Math.PI * Constants.wheelRadius * .0254));
    SmartDashboard.putNumber("Left B Encoder Rotations", m_backLeftEncoder.getPosition()/(2 * Math.PI * Constants.wheelRadius * .0254));
    SmartDashboard.putNumber("Right F Encoder Rotations", m_frontRightEncoder.getPosition()/(2 * Math.PI * Constants.wheelRadius * .0254));
    SmartDashboard.putNumber("Right B Encoder Rotations", m_backRightEncoder.getPosition()/(2 * Math.PI * Constants.wheelRadius * .0254));

    SmartDashboard.putNumber("Left F Encoder", m_frontLeftEncoder.getPosition());
    SmartDashboard.putNumber("Left B Encoder", m_backLeftEncoder.getPosition());
    SmartDashboard.putNumber("Right F Encoder", m_frontRightEncoder.getPosition());
    SmartDashboard.putNumber("Right B Encoder", m_backRightEncoder.getPosition());
  }
}
