package frc.robot.drive;

import frc.robot.Robot;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.drive.MecanumDrive;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;


public class MecanumFieldCentricCommand extends CommandBase {
    public MecanumFieldCentricCommand() {
        addRequirements(Robot.mecanumSubsystem);
    }

    @Override
    public void execute() {
        double x = 0; // x-axis motion-right (+), left (-)
        double y = 0; // y-axis motion-forward (+), backward (-)
        double z = 0; // z-axis motion-clockwise (+), counterclockwise (-)
        if (Math.abs(Robot.joystick.getZ()) > .15) {
            z = Robot.joystick.getZ(); // z-axis threshold
            //Robot.mecanumExample.zLargest(z);
        } 
        if (Math.abs(Robot.joystick.getY()) > .15) {
            y = Robot.joystick.getY(); // y-axis threshold
            //Robot.mecanumExample.yLargest(y);
        }
        if (Math.abs(Robot.joystick.getX()) > .15) {
            x = Robot.joystick.getX(); // x-axis threshold
            //Robot.mecanumExample.xLargest(x);
        }
        else {
            //Robot.mecanumExample.xLargest(0);
        }

        Robot.mecanumSubsystem.drive(-y, -x, -z, true);
        //Robot.mecanumExample.drive(x, y, z, true);
    }


}
