package frc.robot.drive;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class DriveXYZCommand extends CommandBase {
    double forBack;
    double leftRight;
    double turn;
    
    public DriveXYZCommand(double x, double y, double z) {
        addRequirements(Robot.mecanumSubsystem);
        forBack = x;
        leftRight = y;
        turn = z;
    }

    @Override
    public void execute() {
        Robot.mecanumSubsystem.drive(forBack, leftRight, turn, false);
    }

    @Override
    public boolean isFinished() {
        return false;
    }

    @Override
    public void end(boolean interrupted) {
        Robot.mecanumSubsystem.drive(0, 0, 0, false);
    }
}
