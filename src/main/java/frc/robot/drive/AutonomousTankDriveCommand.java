package frc.robot.drive;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class AutonomousTankDriveCommand extends  CommandBase {
    double currentDistance = 0;
    double distanceError = 0;
    double targetDistance = 0;

    public AutonomousTankDriveCommand(double distance) {
        //addRequirements(Robot.tankDrive);
        targetDistance = distance;
    }

    @Override
    public void execute() {
        distanceError = targetDistance - getCurrentDistance();
        //Robot.tankDrive.autoDrive(.40 * (distanceError / Math.abs(distanceError)), 0);
        
        
        SmartDashboard.putNumber("Current Distance Auto", getCurrentDistance());
        SmartDashboard.putNumber("Current Error", distanceError);
    }

    public double getCurrentDistance() {
        return 0;//Robot.tankDrive.getLeftEncoder() * Constants.TANK_DRIVE_ENCODER_RATIO;
    }

    @Override
    public boolean isFinished() {
        return Math.abs(distanceError) < .1;
    }

    @Override
    public void end(boolean interrupted) {
        //Robot.tankDrive.drive(0, 0);
    }
}
