package frc.robot.gripper;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class AutoGripperReverse extends CommandBase {
    boolean fast = false;
    public AutoGripperReverse() {
        addRequirements(Robot.gripperSubsystem);
    }

    public AutoGripperReverse(boolean goFast) {
        addRequirements(Robot.gripperSubsystem);
        fast = goFast;
    }

    @Override
    public void execute() {
        if (fast) {
            Robot.gripperSubsystem.setReverseFast();
        }
        else {
            Robot.gripperSubsystem.setReverse();
        }
    }

    @Override
    public boolean isFinished() {
       return false;
    }

    @Override
    public void end(boolean interrupted) {
        Robot.gripperSubsystem.stop();
        fast = false;
    }
}
