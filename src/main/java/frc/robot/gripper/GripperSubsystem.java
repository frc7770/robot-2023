package frc.robot.gripper;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;

import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class GripperSubsystem extends SubsystemBase {
    public boolean isRunning = false;

    public CANSparkMax rightGripperMotor;
    public CANSparkMax leftGripperMotor;

    public GripperSubsystem() {
        rightGripperMotor = new CANSparkMax(Constants.RIGHT_INTAKE_MOTOR, CANSparkMax.MotorType.kBrushless);
        leftGripperMotor = new CANSparkMax(Constants.LEFT_INTAKE_MOTOR, CANSparkMax.MotorType.kBrushless);

        rightGripperMotor.setInverted(true);

        leftGripperMotor.setIdleMode(IdleMode.kBrake);
        rightGripperMotor.setIdleMode(IdleMode.kBrake);

    }

    public void setSpeed() {
        //if (!(Robot.colorSensor0.hasCube() || Robot.colorSensor0.hasCone())) {
        rightGripperMotor.set(Constants.GRIPPER_SPEED * .5);
        leftGripperMotor.set(Constants.GRIPPER_SPEED * .5);
        //isRunning = true;
        //}
        //else {
            //stop();
        //}
        
    }

    public void setReverse() {
        rightGripperMotor.set(-Constants.GRIPPER_SPEED * Robot.gripperSpeed.getSelected());
        leftGripperMotor.set(-Constants.GRIPPER_SPEED * Robot.gripperSpeed.getSelected());
        //isRunning = true;
    }

    public void setReverseFast() {
        rightGripperMotor.set(-Constants.GRIPPER_SPEED);
        leftGripperMotor.set(-Constants.GRIPPER_SPEED);
    }

    public void stop() {
        rightGripperMotor.set(0);
        leftGripperMotor.set(0);
        //isRunning = false;
    }

    /* public void gripperLEDs() {
        if (Robot.colorSensor0.hasCone()) {
            Robot.testLEDs.segments(Color.kYellow, 31, 27);
        }
        else if (Robot.colorSensor0.hasCube()) {
            Robot.testLEDs.segments(Color.kPurple, 31, 27);
        }
        else {
            Robot.testLEDs.segments(Color.kRed, 31, 27);
        }
    }

    public boolean isRunning() {
        return isRunning;
    } */

    /* public boolean isRunning() {
        return isRunning;
    } */

    public void logToDashboard() {
        
    }
}

