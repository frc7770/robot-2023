package frc.robot.gripper;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class TestGripperCommand extends CommandBase {
    public TestGripperCommand() {
        addRequirements(Robot.gripperSubsystem);
    }

    @Override
    public void execute() {
        /* if (Robot.joystick.getPOV() == 180) {
            //System.out.println("Spin Intake");
            Robot.gripperSubsystem.setSpeed();
        }
        else if(Robot.joystick.getPOV() == 0) {
            Robot.gripperSubsystem.setReverse();
        }
        if (Math.abs(Robot.xboxController1.getRightY()) > .1) {
            Robot.gripperSubsystem.setReverseFast();
        } */
        if (Robot.xboxController1.getRightTriggerAxis() > 0.1) {
            Robot.gripperSubsystem.setSpeed();
        }
        else if (Robot.xboxController1.getLeftTriggerAxis() > 0.1) {
            if (Robot.armSubsystem.elbowAngleDegrees() > 42.5) {
                Robot.gripperSubsystem.setReverseFast();
            }
            else {
                Robot.gripperSubsystem.setReverse();
            }
        }
        else {
            Robot.gripperSubsystem.stop();
        }
    }

    @Override
    public boolean isFinished() {
       return false;
    }
}
