package frc.robot.gripper;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class GripperReverse extends CommandBase {
    public GripperReverse() {
        addRequirements(Robot.gripperSubsystem);
    }

    @Override
    public void execute() {
        if (Robot.armSubsystem.elbowAngleDegrees() > 42.5) {
            Robot.gripperSubsystem.setReverseFast();
        }
        else {
            Robot.gripperSubsystem.setReverse();
        }
    }

    @Override
    public boolean isFinished() {
       return false;
    }

    @Override
    public void end(boolean interrupted) {
        Robot.gripperSubsystem.stop();
    }
}
