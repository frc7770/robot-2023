package frc.robot.gripper;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class GripperIntake extends CommandBase {
    public GripperIntake() {
        addRequirements(Robot.gripperSubsystem);
    }

    @Override
    public void execute() {
        Robot.gripperSubsystem.setSpeed();
    }

    @Override
    public boolean isFinished() {
       return false;
    }

    @Override
    public void end(boolean interrupted) {
        Robot.gripperSubsystem.stop();
    }
}
