package frc.robot.gripper;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class AutonomousGripperCommand extends CommandBase {
  public AutonomousGripperCommand() {
    addRequirements(Robot.gripperSubsystem);
  }
  @Override
  public void execute() {
    Robot.gripperSubsystem.setSpeed();
  }

  @Override
  public void end(boolean interrupted) {
    Robot.gripperSubsystem.stop();
  }

  @Override
  public boolean isFinished() {
    return false; //Robot.colorSensor0.hasCube() || Robot.colorSensor0.hasCone();
  }
}
