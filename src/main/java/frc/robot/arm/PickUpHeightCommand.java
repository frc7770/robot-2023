package frc.robot.arm;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class PickUpHeightCommand extends CommandBase {
    public double shoulderAngle = 30;
    public double elbowAngle = -35; //30

    public PickUpHeightCommand() {
        addRequirements(Robot.armSubsystem);
    }

    public PickUpHeightCommand(double angle) {
        addRequirements(Robot.armSubsystem);
        elbowAngle = angle;
    }

    @Override
    public void execute() {
        double elbowAngleError = elbowAngle - Robot.armSubsystem.elbowAngleDegrees();

        Robot.armSubsystem.setElbowSpeed(elbowAngleError * .15);
    }

    @Override
    public boolean isFinished() {
        return Math.abs(elbowAngle - Robot.armSubsystem.elbowAngleDegrees()) < 2;//(Math.abs(shoulderAngle - Robot.armSubsystem.shoulderAngleDegrees()) < 2 && Math.abs(elbowAngle - Robot.armSubsystem.elbowAngleDegrees()) < 2.5);
    }

    @Override
    public void end(boolean interrupted) {
        //Robot.armSubsystem.setShoulderSpeed(0);
        Robot.armSubsystem.setElbowSpeed(0);
    }
}
