package frc.robot.arm;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class MidHeightPlaceCommand extends CommandBase {
    public double shoulderAngle = 65;
    public double elbowAngle = 25;

    public MidHeightPlaceCommand() {
        addRequirements(Robot.armSubsystem);
    } 

    public MidHeightPlaceCommand(double removeElbowAngle, double removeShoulderAngle) {
        addRequirements(Robot.armSubsystem);
        elbowAngle -= removeElbowAngle;
    }

    @Override
    public void execute() {
        double elbowAngleError = elbowAngle - Robot.armSubsystem.elbowAngleDegrees();

        Robot.armSubsystem.setElbowSpeed(elbowAngleError * .15);  
    }

    @Override
    public boolean isFinished() {
        return Math.abs(elbowAngle - Robot.armSubsystem.elbowAngleDegrees()) < 2; //(Math.abs(shoulderAngle - Robot.armSubsystem.shoulderAngleDegrees()) < 2 && Math.abs(elbowAngle - Robot.armSubsystem.elbowAngleDegrees()) < 2);
    }

    @Override
    public void end(boolean interrupted) {
        //Robot.armSubsystem.setShoulderSpeed(0);
        Robot.armSubsystem.setElbowSpeed(0);
    }
}
