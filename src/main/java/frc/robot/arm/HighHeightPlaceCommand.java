package frc.robot.arm;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class HighHeightPlaceCommand extends CommandBase {
    public double shoulderAngle = 61;
    public double elbowAngle = 51;

    public HighHeightPlaceCommand() {
        addRequirements(Robot.armSubsystem);
    }

    @Override
    public void execute() {
        //double shoulderAngleError = shoulderAngle - Robot.armSubsystem.shoulderAngleDegrees();
        double elbowAngleError = elbowAngle - Robot.armSubsystem.elbowAngleDegrees();

        //Robot.armSubsystem.setShoulderSpeed(-shoulderAngleError * 1.5);
        Robot.armSubsystem.setElbowSpeed(elbowAngleError * .15);
    }

    @Override
    public boolean isFinished() {
        return Math.abs(elbowAngle - Robot.armSubsystem.elbowAngleDegrees()) < 1;
    }

    @Override
    public void end(boolean interrupted) {
        //Robot.armSubsystem.setShoulderSpeed(0);
        Robot.armSubsystem.setElbowSpeed(0);
    }
}
