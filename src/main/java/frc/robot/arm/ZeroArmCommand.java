package frc.robot.arm;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class ZeroArmCommand extends CommandBase {
    double degree = 0;
    public ZeroArmCommand() {
        addRequirements(Robot.armSubsystem);
    }

    public ZeroArmCommand(double absDegree) {
        addRequirements(Robot.armSubsystem);
        degree = absDegree;
    }

    public void execute() {
        Robot.armSubsystem.zeroElbowEncoder(degree);
    }

    @Override
    public boolean isFinished() {
        return true;
    }

    @Override
    public void end(boolean interrupted) {
        degree = 0;
    }
}