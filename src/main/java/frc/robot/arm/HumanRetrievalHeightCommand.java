package frc.robot.arm;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class HumanRetrievalHeightCommand extends CommandBase {
    public double shoulderAngle = 0;
    public double elbowAngle = 0;

    public HumanRetrievalHeightCommand() {
        addRequirements(Robot.armSubsystem);
    }

    @Override
    public void execute() {
        //double shoulderAngleError = shoulderAngle - Robot.armSubsystem.shoulderAngle();
        //double elbowAngleError = elbowAngle - Robot.armSubsystem.elbowAngle();

        //Robot.armSubsystem.setShoulderSpeed(shoulderAngleError * .05);
        //Robot.armSubsystem.setElbowSpeed(elbowAngleError * .05);
    }

    @Override
    public boolean isFinished() {
        return (Math.abs(elbowAngle - Robot.armSubsystem.elbowAngle()) < .1); //Math.abs(shoulderAngle - Robot.armSubsystem.shoulderAngle()) < .1 && 
    }
}
