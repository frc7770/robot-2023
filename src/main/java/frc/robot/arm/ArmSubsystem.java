package frc.robot.arm;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;
import com.revrobotics.CANSparkMax;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.CANSparkMax.IdleMode;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.CAN;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.DutyCycleEncoder;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import frc.robot.Robot;
import edu.wpi.first.wpilibj2.command.SubsystemBase;


/**
 * Spins a pair of wheels that launches the ball at the target
 */
public class ArmSubsystem extends SubsystemBase {
    public VictorSPX shoulderMotor;
    public CANSparkMax elbowMotor;
    public DutyCycleEncoder shoulderEncoder;

    public RelativeEncoder elbowEncoder;
    public double prevShoulderLocation;

    public boolean removeLimit = false;
    public double maxX = Constants.ARM_MAX_X_DISTANCE;
    public double maxY = Constants.ARM_MAX_Y_DISTANCE;
    public double shoulderLength = Constants.SHOULDER_ARM_LENGTH;
    public double elbowLength = Constants.ELBOW_ARM_LENGTH;
    public double armToBase = Constants.ARM_TO_EDGE_LENGTH;
    public double armToGround = Constants.ARM_TO_FLOOR_LENGTH;
    public double gripperLength = Constants.GRIPPER_LENGTH;

    public ArmSubsystem() {
        //shoulderMotor = new VictorSPX(Constants.SHOULDER_MOTOR);
        elbowMotor = new CANSparkMax(Constants.ELBOW_MOTOR, MotorType.kBrushless);

        //shoulderMotor.setInverted(true);
        elbowMotor.setInverted(true);

        elbowEncoder = elbowMotor.getEncoder(); 

        //shoulderEncoder = new DutyCycleEncoder(1); //REV-11-1271'

        elbowMotor.setIdleMode(IdleMode.kBrake);
        elbowEncoder.setPosition(55 * Constants.ELBOW_ENCODER_RATIO);
    }

    public void setElbowSpeed(double speed) {
        /* if (speed > 0 && Math.toDegrees(elbowAngle()) < -60) { //Math.toDegrees(shoulderAngle()) < 52.5 && 
            elbowMotor.set(0);
        } */
        if (speed > 0 && elbowAngleDegrees() > 55) { // && Math.toDegrees(shoulderAngle()) > 60
            elbowMotor.set(0);
        }
        else if (speed < 0 && elbowAngleDegrees() < -25 && !(Robot.turretSubsystem.isRotated())) {
            elbowMotor.set(0);
        }
        else if (speed < 0 && elbowAngleDegrees() < -45 && Robot.turretSubsystem.isRotated()) {
            elbowMotor.set(0);
        }
        else {
            elbowMotor.set(speed * Constants.ARM_SPEED);
        }

    }

    
    /* public void setShoulderSpeed(double speed) {
        if (speed > 0 && Math.toDegrees(shoulderAngle()) < 36) { //36
            shoulderMotor.set(ControlMode.PercentOutput, 0);
        }
        else if (speed > 0 && Math.toDegrees(shoulderAngle()) < 55 && elbowAngleDegrees() < 10) { //55
            shoulderMotor.set(ControlMode.PercentOutput, 0);
        }
        else if (speed < 0 && Math.toDegrees(shoulderAngle()) > 65) { //65
            shoulderMotor.set(ControlMode.PercentOutput, 0);
        }
        else {
            shoulderMotor.set(ControlMode.PercentOutput, speed);
            System.out.println(speed);
        }
    } */

    public void zeroElbowEncoder(double angle) {
        elbowEncoder.setPosition(angle * Constants.ELBOW_ENCODER_RATIO);

    }

    public double shoulderAngle() {
        /*if((shoulderEncoder.getDistance() - prevShoulderLocation) > .6) {
            return (prevShoulderLocation - .566) / Constants.SHOULDER_ENCODER_RATIO;
        }  
        else if */
        return 0;//Math.toRadians((shoulderEncoder.getDistance() - .553) / Constants.SHOULDER_ENCODER_RATIO);
    }

    public double shoulderAngleDegrees() {
        return 0; //Math.toDegrees(shoulderAngle());
    }

    public double elbowAngle() {
        return Math.toRadians(elbowEncoder.getPosition() / Constants.ELBOW_ENCODER_RATIO);
    }

    public double elbowAngleDegrees() {
        return Math.toDegrees(elbowAngle());
    }

    public double XDistance() {
        double shoulderX = Math.cos(shoulderAngle()) * shoulderLength;
        double elbowX = Math.sin(elbowAngle()) * elbowLength;
        //if (Robot.turretSubsystem.rotated90()) {
            //return shoulderX + elbowX;
        //}
        return shoulderX + elbowX - armToBase + gripperLength;
    }

    public double YDistance() {
        double shoulderY = Math.sin(shoulderAngle()) * shoulderLength;
        double elbowY = -Math.cos(elbowAngle()) * elbowLength;
        return shoulderY + elbowY + armToGround;
    }

    public void logToDashboard() {
        //SmartDashboard.putNumber("Through Bore Encoder Position", shoulderEncoder.getDistance());
        SmartDashboard.putNumber("Elbow Encoder Clicks", elbowEncoder.getPosition());
        SmartDashboard.putNumber("Elbow Angle", Math.toDegrees(elbowAngle()));
        //SmartDashboard.putNumber("Shoulder Angle", Math.toDegrees(shoulderAngle()));
        ////SmartDashboard.putNumber("Shoulder X Distnace", Math.cos(shoulderAngle()) * shoulderLength);
        //SmartDashboard.putNumber("Shoulder Y Distnace", Math.sin(shoulderAngle()) * shoulderLength);
        //SmartDashboard.putNumber("Elbow X Distnace", Math.sin(elbowAngle()) * elbowLength);
        //SmartDashboard.putNumber("Elbow Y Distnace", -Math.cos(elbowAngle()) * elbowLength);
        //SmartDashboard.putNumber("X Distance", XDistance());
        //SmartDashboard.putNumber("Y Distance", YDistance());
    }
}

