package frc.robot.arm;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class StowArmCommand extends CommandBase {
    public double shoulderAngle = 63;
    public double elbowAngle = 54; //0
    public double turretAngle = 0;

    public StowArmCommand() {
        addRequirements(Robot.armSubsystem);
    }

    @Override
    public void execute() {
        double elbowAngleError = elbowAngle - Robot.armSubsystem.elbowAngleDegrees();
        Robot.armSubsystem.setElbowSpeed(elbowAngleError * .15);
    }

    @Override
    public boolean isFinished() {
        return Math.abs(elbowAngle - Robot.armSubsystem.elbowAngleDegrees()) < 1.5; //(Math.abs(shoulderAngle - Robot.armSubsystem.shoulderAngleDegrees()) < 2 && Math.abs(elbowAngle - Robot.armSubsystem.elbowAngleDegrees()) < 2);
    }

    @Override
    public void end(boolean interrupted) {
        //Robot.armSubsystem.setShoulderSpeed(0);
        Robot.armSubsystem.setElbowSpeed(0);
    }
}
