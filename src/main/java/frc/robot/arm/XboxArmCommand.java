package frc.robot.arm;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class XboxArmCommand extends CommandBase {

    public XboxArmCommand() {
        addRequirements(Robot.armSubsystem);
    }

    @Override
    public void execute() {
        //double speed = Robot.xboxController1.getRightY(); //Constants.ARM_SPEED * 
        double speed2 = Robot.xboxController1.getLeftY(); //Constants.ARM_SPEED * 
        //double speed2 = Robot.joystick.getThrottle();
       
        /* if (Robot.turretSubsystem.isRotated()) {
        }
        else if (Math.abs(speed) < .05) {
            Robot.armSubsystem.setShoulderSpeed(0);
        }
        else {
            Robot.armSubsystem.setShoulderSpeed(speed);
        } */

        if (Math.abs(speed2) < .20) {
            Robot.armSubsystem.setElbowSpeed(0);
        }
        else {
            Robot.armSubsystem.setElbowSpeed(-speed2);
        }
        
    }
    
    @Override
    public boolean isFinished() {
       return false;
    }
}

