package frc.robot.photonvision;

import org.photonvision.PhotonCamera;
import org.photonvision.targeting.PhotonTrackedTarget;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class PhotonVision extends SubsystemBase {

    private PhotonCamera camera = new PhotonCamera("USB2.0_HD_UVC_WebCam");
    
    @Override
    public void periodic() {
        var result = camera.getLatestResult();
        if (result.hasTargets()) {
            PhotonTrackedTarget target = result.getBestTarget();

            int targetID = target.getFiducialId();
            double pitch = target.getPitch();
            double skew = target.getSkew();
            double yaw = target.getYaw();
            double area = target.getArea();


            SmartDashboard.putNumber("Target ID", targetID);
            SmartDashboard.putNumber("Target Pitch", pitch);
            SmartDashboard.putNumber("Target Skew", skew);
            SmartDashboard.putNumber("Target Yaw", yaw);
            SmartDashboard.putNumber("Target Area", area);

        }
    }
}


