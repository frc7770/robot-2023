package frc.robot.sensors;

import com.kauailabs.navx.frc.AHRS;
import com.kauailabs.navx.frc.AHRS.SerialDataType;

import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.SerialPort;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Gyroscope {
    AHRS gyro;

    public Gyroscope() {
        //stick = new Joystick(0);
        try {
      /***********************************************************************
       * navX-MXP:
       * - Communication via RoboRIO MXP (SPI, I2C, TTL UART) and USB.            
       * - See http://navx-mxp.kauailabs.com/guidance/selecting-an-interface.
       * 
       * navX-Micro:
       * - Communication via I2C (RoboRIO MXP or Onboard) and USB.
       * - See http://navx-micro.kauailabs.com/guidance/selecting-an-interface.
       * 
       * Multiple navX-model devices on a single robot are supported.
       ************************************************************************/
            //ahrs = new AHRS(SerialPort.Port.kUSB1);
            gyro = new AHRS(SerialPort.Port.kMXP, SerialDataType.kProcessedData, (byte)50);
            gyro.enableLogging(true);
        } catch (RuntimeException ex ) {
            DriverStation.reportError("Error instantiating navX MXP:  " + ex.getMessage(), true);
        }
        Timer.delay(1.0);
      //UsbCamera cam = CameraServer.getInstance().startAutomaticCapture();
      //cam.setResolution(640, 480);        
    }

    public void zeroYaw() {
        gyro.zeroYaw();
    }

    public void reset() {
        gyro.reset();
    }

    public double change() {
        return gyro.getRoll();
    }

    public double getAngle() {
        return gyro.getAngle();
    }

    public Rotation2d getRotation() {
        return gyro.getRotation2d();//.minus(Rotation2d.fromDegrees(90));
        
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Gyro Change", change());
    }
}
