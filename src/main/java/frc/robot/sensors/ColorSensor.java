// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.sensors;

import edu.wpi.first.wpilibj.I2C;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.util.Color;

import com.revrobotics.ColorMatch;
import com.revrobotics.ColorMatchResult;
import com.revrobotics.ColorSensorV3;

/** Add your docs here. */
public class ColorSensor {

    protected final I2C.Port i2cPort = I2C.Port.kOnboard;
    protected ColorSensorV3 m_colorSensor = new ColorSensorV3(i2cPort);
    private final ColorMatch m_colorMatcher = new ColorMatch();
    public boolean hasCube = false;
    public boolean hasCone = false;
    public static enum BallColor{PURPLE, YELLOW, OTHER, NONE}

    public ColorSensor() {
        m_colorMatcher.addColorMatch(Color.kPurple);
        m_colorMatcher.addColorMatch(Color.kGoldenrod);
    }

    public ColorSensor.BallColor getBall() {
        Color detectedColor = m_colorSensor.getColor();
        SmartDashboard.putString("Color Guess", detectedColor.toString());
        ColorMatchResult match = m_colorMatcher.matchClosestColor(detectedColor);
        SmartDashboard.putString("Closest Color", match.color.toString());
        // If low confidence return none
        SmartDashboard.putNumber("Confidence", match.confidence);
        if (match.confidence < .59) {
            hasCube = false;
            hasCone = false;
            return ColorSensor.BallColor.NONE;
        }
        if(match.color == Color.kPurple && match.confidence > .59) {
            hasCube = true;
            hasCone = false;
            //ballColor = Color.kBlue;
            return ColorSensor.BallColor.PURPLE;
        }
        else if(match.color == Color.kGoldenrod && match.confidence > .64) { /* match.color == Color.kOrange  */ 
            //ballColor = Color.kBlue; 
            hasCube = false;
            hasCone = true;
            return ColorSensor.BallColor.YELLOW;
        }
        /*
        else if(match.color == Color.kBlue && match.confidence > .35) {
            hasBall = true;
            //ballColor = Color.kBlue;
            return ColorSensor.BallColor.BLUE;
        }
        else if(match.color == Color.kRed && match.confidence > .4) {
            //ballColor = Color.kBlue;
            hasBall = true;
            return ColorSensor.BallColor.RED; */
        else 
            hasCube = false;
            hasCone = false;
            return ColorSensor.BallColor.OTHER;
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Blue", m_colorSensor.getBlue());
        SmartDashboard.putNumber("Red", m_colorSensor.getRed());
        SmartDashboard.putNumber("Green", m_colorSensor.getGreen());
        SmartDashboard.putString("BallColor", getBall().name());
        SmartDashboard.putBoolean("Has Cube", hasCube());
        SmartDashboard.putBoolean("Has Cone", hasCone());
    }

    public boolean hasCube() {
        return hasCube;
    }
    public boolean hasCone() {
        return hasCone;
    }

    /* public Color ballColor() {
        return ballColor;
    } */

}
