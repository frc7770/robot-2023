package frc.robot.sensors;

import edu.wpi.first.cscore.UsbCamera;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Pose3d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.GenericHID.RumbleType;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Robot;

public class LimeLight {
    double[] pose = new double[3];
    // If target detects a target
    public boolean detectTarget() {
        double tv = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tv").getDouble(0);
        // Testing for if the limelight finds a target
        if (tv == 1) {
            return true;
        }
        else {
            return false;
        }      
    }
    // x angle of the target
    public double getTX() {
        double tx = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tx").getDouble(0);
        return tx; 
    }
    // y angle of the target
    public double getTY() {
        double ty = NetworkTableInstance.getDefault().getTable("limelight").getEntry("ty").getDouble(0);
        return ty;  
    }    
    public double xPose() {
        double mountDegrees = 0;
        double mountHeight = 51;
        double targetHeight = 58;
        double angleToTarget = getTY();
        
        double angle = mountDegrees + angleToTarget;
        double degreesToRadians = angle * (Math.PI / 180);

        double distanceToTarget = (targetHeight - mountHeight)/Math.tan(degreesToRadians);
        return distanceToTarget;
    }

    public double yPose() {
        return 0;
    }

    public long getID() {
        long ID = NetworkTableInstance.getDefault().getTable("limelight").getEntry("tid").getInteger(-1);
        return ID;
    }

    public Pose2d getPose() {
        pose = NetworkTableInstance.getDefault().getTable("limelight").getEntry("botpose").getDoubleArray(new double[6]);
        return new Pose2d(
            pose[0],
            pose[1],
            Rotation2d.fromDegrees(pose[5])
        );
    }

    public boolean inXSpot() {
        return Math.abs(getPose().getX() - 5) < .75;
    }

    public boolean inYSpot() {
        return Math.abs(getPose().getY() - 1) < .75;
    }

    public boolean inRotationSpot() {
        return Math.abs((getPose().getRotation()).getDegrees()) < 5 && Math.abs((getPose().getRotation()).getDegrees()) != 0;
    }

    // Logging the values to the dashboard
    public void logToDashboard() {
        SmartDashboard.putBoolean("LimeLightV", detectTarget());
        SmartDashboard.putNumber("LimeLight X Angle Offset", getTX());
        SmartDashboard.putNumber("LimeLight Y Angle Offset", getTY());
        SmartDashboard.putNumber("Tag ID", getID());
        SmartDashboard.putNumber("X Distance To Target", xPose());
        SmartDashboard.putString("Pose of Robot from April Tag", getPose().toString());
        SmartDashboard.putBoolean("In X Sweet Spot", inXSpot());
        SmartDashboard.putBoolean("In Y Sweet Spot", inYSpot());
        SmartDashboard.putBoolean("In Rotation Sweet Spot", inRotationSpot());

        if(Robot.field!=null) {
            Robot.field.setRobotPose(getPose());
            //Robot.field.setRobotPose(m_odometry.getPoseMeters().getX(), m_odometry.getPoseMeters().getY(), Rotation2d.fromDegrees(22));
        }
        SmartDashboard.putData(Robot.field);
    }

}
