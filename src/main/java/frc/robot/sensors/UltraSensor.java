package frc.robot.sensors;

import edu.wpi.first.wpilibj.AnalogInput;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class UltraSensor {
    AnalogInput sensor;

    public UltraSensor() {
        sensor = new AnalogInput(0);
    }

    public double getVoltage() {
        return sensor.getVoltage();
    }

    public double getDistance() {
        return (getVoltage()) / .00977;
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Ultrasonic Voltage", getVoltage());
        SmartDashboard.putNumber("Ultrasonic Distance", getDistance());
    }
}
