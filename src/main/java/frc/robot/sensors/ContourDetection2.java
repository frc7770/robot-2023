package frc.robot.sensors;

import java.util.ArrayList;

import org.opencv.core.MatOfKeyPoint;
import org.opencv.core.MatOfPoint;

import edu.wpi.first.vision.VisionPipeline;

public interface ContourDetection2 extends VisionPipeline {
    public ArrayList<MatOfPoint> filterContoursOutput();
}
