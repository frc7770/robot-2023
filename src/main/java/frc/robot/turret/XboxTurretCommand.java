package frc.robot.turret;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class XboxTurretCommand extends CommandBase {
    public double shoulderAngleRotated = 63;
    public XboxTurretCommand() {
        addRequirements(Robot.turretSubsystem);
    }
    @Override
    public void execute() {
        if (Robot.xboxController1.getPOV() == 90 || Robot.xboxController1.getPOV() == 45 || Robot.xboxController1.getPOV() == 135) {
            Robot.turretSubsystem.turnTurret(Constants.TURRET_SPEED);
        }
        else if (Robot.xboxController1.getPOV() == 270 || Robot.xboxController1.getPOV() == 225 || Robot.xboxController1.getPOV() == 315) {
            Robot.turretSubsystem.turnTurret(-Constants.TURRET_SPEED);
        }/*
        else if (Robot.joystick.getPOV() == 90) {
            Robot.turretSubsystem.turnTurret(Constants.TURRET_SPEED);
        }
        else if(Robot.joystick.getPOV() == 270) {
            Robot.turretSubsystem.turnTurret(-Constants.TURRET_SPEED);
        } */
        else {
            Robot.turretSubsystem.turnTurret(0);
        }
    }
    
    @Override
    public boolean isFinished() {
       return false;
    }
}