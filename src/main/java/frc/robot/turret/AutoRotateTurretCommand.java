package frc.robot.turret;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Robot;

public class AutoRotateTurretCommand extends CommandBase {
    public double desiredAngle = 0;
    public boolean camera = false;

    public AutoRotateTurretCommand(double angle) { //double angle
        addRequirements(Robot.turretSubsystem);
        desiredAngle = angle;
    }

    public AutoRotateTurretCommand(double angle, boolean useCamera) { //double angle
        addRequirements(Robot.turretSubsystem);
        desiredAngle = 0;
        camera = true;
    }

    @Override
    public void execute() {
        SmartDashboard.putBoolean("Camera On", camera);
        double cameraSpeed = SmartDashboard.getNumber("Angle to Cube", 0); //("Angle to Cone", 0);
        if (camera) {
            if (cameraSpeed == 0) {

            }
            else if (Math.abs(cameraSpeed) > 120) {
                Robot.turretSubsystem.turnTurret((cameraSpeed)/Math.abs(cameraSpeed) * 120 * .00075);
            }
            else {
                Robot.turretSubsystem.turnTurret(cameraSpeed * .00075);
            }
            System.out.println("Turning Turret Auto " + (cameraSpeed * .00075));
        }
        else {
            double turretAngleError = desiredAngle - Robot.turretSubsystem.getAngle();
            if (Math.abs(turretAngleError * .02) > .6) {
                Robot.turretSubsystem.turnTurret(Math.abs(turretAngleError)/turretAngleError * .6);
            }
            else {
                Robot.turretSubsystem.turnTurret(turretAngleError * .02);
            }
        }
    }

    @Override
    public boolean isFinished() {
        if(camera) {
            return (Math.abs(SmartDashboard.getNumber("Angle to Cube", 0)) < 20 && Math.abs(SmartDashboard.getNumber("Angle to Cube", 0)) != 0);
        }
        else if (!(camera)) {
            return (Math.abs(desiredAngle - Robot.turretSubsystem.getAngle()) < .1);
        }
        return false;//SmartDashboard.getNumber("Angle to Cube", 0) < 10 && SmartDashboard.getNumber("Angle to Cube", 0) > -20;
    }

    @Override
    public void end(boolean interrupted) {
        desiredAngle = 0;
    }
}
