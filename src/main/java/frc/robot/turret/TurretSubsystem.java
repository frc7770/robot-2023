package frc.robot.turret;

import com.revrobotics.AlternateEncoderType;
import com.revrobotics.CANSparkMax;
import com.revrobotics.EncoderType;
import com.revrobotics.RelativeEncoder;
import com.revrobotics.SparkMaxAlternateEncoder.Type;

import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import frc.robot.Robot;
import edu.wpi.first.wpilibj2.command.SubsystemBase;


/**
 * Spins a pair of wheels that launches the ball at the target
 */
public class TurretSubsystem extends SubsystemBase {
    public CANSparkMax turret;
    public RelativeEncoder turretEncoder;
    

    public TurretSubsystem() {
        turret = new CANSparkMax(Constants.TURRET_MOTOR, CANSparkMax.MotorType.kBrushless);
        turret.setIdleMode(CANSparkMax.IdleMode.kBrake);
        turretEncoder = turret.getEncoder();
    }

    public void setZero() {
        turretEncoder.setPosition(0);
    }

    public void turnTurret(double speed) {
        System.out.println(speed);
        if (getAngle() > 210 && speed > 0) {
            turret.set(0);
        }
        else if (getAngle() < -210 && speed < 0) {
            turret.set(0);
        }
        else {
            turret.set(speed);
        }        
    }

    public double getAngle() {
        return (turretEncoder.getPosition() * Constants.TURRET_ENCODER_RATIO) % 360;
    }

    public boolean rotated90Left() {
        return getAngle() < -60 && getAngle() > -120;
    }

    public boolean rotated90Right() {
        return getAngle() > 60 && getAngle() < 120;
    }

    public boolean isRotated() {
        return (rotated90Left() || rotated90Right()) || Math.abs(getAngle()) > 165;
    }

    public void logToDashboard() {
        SmartDashboard.putNumber("Encoder Clicks", turretEncoder.getPosition());
        SmartDashboard.putNumber("Turret Angle", getAngle());
        //SmartDashboard.putNumber("Angle", getAngle());
    }
}

