package frc.robot.turret;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.Robot;

public class ZeroTurretAngle extends CommandBase {
    
    public ZeroTurretAngle() {
        addRequirements(Robot.turretSubsystem);
    }
    @Override
    public void execute() {
        Robot.turretSubsystem.setZero();
    }
    
    @Override
    public boolean isFinished() {
       return true;
    }
}