# Robot 2023

CHARGED UP


## Design and Challenge

In CHARGED UP presented by Haas, two competing alliances are invited to process game pieces to
bring energy to their community. Each alliance brings energy to their community by retrieving their game
pieces from substations and scoring it into the grid. Human players provide the game pieces to the
robots from the substations. In the final moments of each match, alliance robots race to dock or engage
with their charge station!
Each match begins with a 15-second autonomous period, during which time alliance robots operate only
on pre-programmed instructions to score points by:
- leaving their community,
- retrieving and scoring game pieces onto the grid,
- docking on or engaging with their charge station.
In the final 2 minutes and 15 seconds of the match, drivers take control of the robots and score points by:
- continuing to retrieve and score their game pieces onto the grid and
- docking on or engaging with their charge station.
The alliance with the highest score at the end of the match wins!

## Electric Subsystems

Subsystems are created for each dynamic system that uses electric motors or pneumatic rams.

### Drive

The drive system is a Mecanum system with four wheel setup that uses four SPARK MAX controllers with motors and encoders.  The Mecanum Drive model which gives the driver the ability to rotate, drive forwards, or drive sideways. The driver is also able to toggle between brake mode and coast mode.

### Arm

The arm has two joints driven by screw drives.  A worm drive motor with external encoder is used that the first joint.  A Neo is used at the second joint.

### Gripper

The gripper picks up both cones and cubes.  It uses green wheels with two Neo motors that turn in opposite directions.  One encoder is used.

### Turret

The turret is the base of the arm and uses a belt drive and Neo motor.  The encoder is used to track the rotation on the turret.

## Pneumatic Subsystems

No pneumatic systems were used on this bot.

## Sensors

### Limelight

Limelight is a plug-and-play smart camera purpose-built for the FIRST Robotics Competition.  Vision control pipelines are created using the included software to tune the camera to recognize a given target.  The system then retrns a tranjectory to that target.  7770 uses the limelight to automate the targetting of the turret and speed of the shooter.

https://limelightvision.io/

### Color Sensor

Describe what the sytem was supposed to do.

https://www.revrobotics.com/rev-31-1557/

This sensor can be used to read and compare colors, and also has a built-in IR (optical) Proximity Sensor and white LED for active target lighting. Supports High Speed I2C Communication (400kHz) as well as auto increment register read which allows the user to return all the color register and status register data in one read command instead of 4 separate read commands.

Version 3 of the REV Color Sensor introduces a new sensor chip by Broadcom due to the end of life of the V1/V2 sensor. Physical form factor is the same as V2, however there are some minor changes to the FTC SDK. Be sure to update to the latest SDK and configure your robot to use the "REV Color Sensor V3". Color values will not be consistent between V2 and V3 sensors.

## Driver Controls

Describe the controls and strategies for the drivers.

## Vendor Dependencies

### Vendor Library A

- [REVLib](https://docs.revrobotics.com/sparkmax/software-resources/spark-max-api-information#c++-and-java) - [https://software-metadata.revrobotics.com/REVLib.json](https://software-metadata.revrobotics.com/REVLib.json)


